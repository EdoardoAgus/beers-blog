<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Models\Magazine;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function homepage() {
        $articles = Article::all();

        return view('homepage', compact('articles'));
    }

    public function create() {
        $magazines = Magazine::all();
        return view('article.form', compact('magazines'));
    }

    public function store(ArticleRequest $req) {
        if ($req->magazine) {
            $magazine = Magazine::find($req->magazine);
        
        $magazine->articles()->create([
            'title'=>$req->input('title'),
            'description'=>$req->input('description'),
            'img'=>$req->file('img')->store('public/img'),
            'user_id'=>Auth::id(),
        ]);
        }
        
        $article = Article::create([
            'title'=>$req->title,
            'description'=>$req->description,
            'img'=>$req->file('img')->store('public/img'),
            'user_id'=>Auth::id(),
        ]);
        return redirect(route('homepage'));
    }

    public function detail(Article $article){

        return view('article.detail', compact('article'));
    }
    
    
}
