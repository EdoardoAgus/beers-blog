<x-layout>


    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="title">Consigliaci una Birra</h1>
            </div>
        </div>
        
    </div>
    <div class="container">
        <div class="row">
            
                
                    @foreach ($articles as $article)
                    <div class="card col-sm-12 col-md-6 col-lg-1 card mt-5 mb-5 ms-3 me-3 p-0" style="width: 18rem; display:inline-block; background-color:brown;">
                        @if ($article->img) 
                            <img src="{{Storage::url($article->img)}}" class="card-img-top card-img" alt="...">
                        @else 
                            <img src="/img/logo_blog.png" class="card-img-top card-img" alt="...">
                        @endif   
                            <div class="card-body card-article">
                            <h5 class="card-title card-title">{{$article->title}}</h5>
                            <p class="card-text">{{$article->description}}</p>
                            <div>
                                <p class="card-text mb-3">Di {{$article->user->name}}</p>
                            </div>
                            <a href="{{route('article.detail', compact("article"))}}" class="btn btn-primary button-nav">Dettaglio</a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-layout>