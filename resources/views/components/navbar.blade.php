<nav class="navbar navbar-expand-lg nav-style">
    <div class="container-fluid">
      <a class=" brand" href="{{route('homepage')}}">BEARBeer</a>
      <button class="navbar-toggler button-nav ms-3 me-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse d-flex align-item-center" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          @guest
          
          <li><a class="dropdown-item nav-text" href="{{route('register')}}">Registrati</a></li>
          <li class="nav-item ms-3 me-3">
          <li><a class="dropdown-item nav-text" href="{{route('login')}}">Accedi</a></li>
          <li class="nav-item ms-3 me-3">
            <a class="nav-link nav-text p-0" aria-current="page" href="{{route('homepage')}}"><i class="fas fa-home"></i></a>
          </li>
          
          @else
          {{-- <a class="nav-text dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Account
          </a> --}}
          {{-- <ul class="dropdown-menu" aria-labelledby="navbarDropdown"> --}}
          <li class="nav-item ms-3 me-3">
            <a class="nav-link nav-text" href="{{route('article.create')}}"><i class="far fa-plus-square"></i></a>
          </li>
          <li class="nav-item ms-3 me-3">
            <a class="nav-link nav-text" href="{{route('magazine.create')}}"><i class="fas fa-book-reader"></i></a>
          </li>
          <a class="nav-link dropdown-toggle nav-text ms-3 me-3" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Ciao, {{Auth::user()->name}}
          </a>
          <ul class="dropdown-menu nav-style" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item nav-text" href="{{route('login')}}">Log-in</a></li>
            <li><a class="dropdown-item nav-text" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('form-logout').submit();">logout</a></li>
              <form method="POST" action="{{route('logout')}}" style="display: none" id="form-logout">
                @csrf
              </form>
            {{-- <li><a class="dropdown-item" href="#">Something else here</a></li> --}}
          </ul>
          {{-- </ul> --}}
          <li class="nav-item ms-3 me-3">
            <a class="nav-link active nav-text  p-0" style="margin-top:10px" aria-current="page" href="{{route('homepage')}}"><i class="fas fa-home"></i></a>
          </li>
          {{-- <li>
            <a class="nav-link dropdown-toggle " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              
            </a>
          </li> --}}
          
          {{-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-text" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Dropdown
            </a> --}}
            
            {{-- <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" ></a></li>
              
              <li><hr class="dropdown-divider"></li>
              
              
            </ul>
          </li> --}}
          {{-- <li class="nav-item">
            <a class="nav-link disabled nav-text" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          </li> --}}
          @endguest
        </ul>
        {{-- <form class="d-flex">
          {{-- <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">Search</button> --}}
        {{-- </form>  --}}
      </div>
    </div>
  </nav>