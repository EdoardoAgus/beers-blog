<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BEARBEER</title>
        {{-- link css e bootstrap--}}
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        {{-- link favicon --}}
        <link rel="shortcut icon" href="/img/bear.ico" type="image/x-icon"/>
        {{-- font awesome --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
        {{-- google fonts --}}
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Karantina:wght@700&family=Lato&family=Montserrat&family=Oswald&family=Raleway&display=swap" rel="stylesheet">

    </head>
    <body>
        <x-navbar>
            
        </x-navbar>
        <div class="container">
            <div class="row d-flex justify-content-center mt-5 mb-5">
                <div class="antialiased body-style">
                    {{$slot}}
                </div>
            </div>
        </div>
        <x-footer>
            
        </x-footer>
        {{-- script js e bootstrap--}}
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>