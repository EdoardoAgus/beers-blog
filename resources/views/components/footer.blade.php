<div class="container-flow mb-0 footer">
        
    <div class="row d-flex justify-content-center">
        <div class="col-sm-12 col-md-8 me-5 ms-5" style="width: 300px">
            <h6 class="social-title">Seguici Sui Social</h6>
            <div class="d-flex justify-content-start"> 
                <i class="fab fa-facebook-square color-facebook me-2 ms-2" style="font-size: 25px"><a href="https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2Faulab%2Fposts%2F1489797351190480"></a></i>
                <i class="fab fa-twitter-square color-twitter me-2 ms-2" style="font-size: 25px"><a href="https://twitter.com/?lang=it"></a></i>
                <i class="fab fa-instagram color-instagram me-2 ms-2" style="font-size: 25px"><a href="https://www.instagram.com/"></a></i>
                <i class="fab fa-linkedin color-linkedin me-2 ms-2" style="font-size: 25px"><a href="https://it.linkedin.com/"></a></i></div>
            </div>
    </div>
    <div class="row  d-flex justify-content-end mt-5" style=" bottom:10px;">
        <div class="col-sm-12 col-md-8">
            {{-- <p class="card-text">Copyright 2021:73755013475</p> --}}
        </div>
    </div>
    
</div>