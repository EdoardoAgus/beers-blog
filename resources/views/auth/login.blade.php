<x-layout>
 
    <div class="container">

        <div class="row mt-5 mb-5">
            <h1 class="title">Login</h1>
            <div class="col-12">
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label card-text">Email address</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value={{old('title')}}>
                      
                    </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label card-text">Password</label>
                      <input type="password" name="password" class="form-control" id="exampleInputPassword1" value={{old('title')}}>
                    </div>
                   
                    <button type="submit" class="btn btn-primary button-filler">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</x-layout>