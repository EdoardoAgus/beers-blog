<x-layout>

    <div class="container mt-5 mb-5">
        <div class="row">
            <h1 class="title">Registrati</h1>
            <div class="col-12 col-md-6">
                <form method="POST" action="{{route('register')}}" >
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label card-text">Nome utente</label>
                        <input type="text" name="name" class="form-control" id="name1" value={{old('title')}}>
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label card-text">Email address</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value={{old('title')}}>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label card-text">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" value={{old('title')}}>
                    </div>
                    <div class="mb-3">
                      <label for="password_confirmation" class="form-label card-text">Conferma Password</label>
                      <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" value={{old('title')}}>
                    </div>
                    
                    <button type="submit" class="btn btn-primary button-filler">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</x-layout>