<x-layout>
   


        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="title">Riviste</h1>
                </div>
            </div>
            
        </div>
        <div class="container">
            <div class="row">
                
                    
                @foreach ($magazines as $magazine)
                    <div class="card col-sm-12 col-md-6 col-lg-1 card mt-5 mb-5 ms-3 me-3 p-0" style="width: 18rem; display:inline-block; background-color:brown;">
                            @if ($magazine->img) 
                                <img src="{{Storage::url($magazine->img)}}" class="card-img-top card-img" alt="...">
                            @else 
                                <img src="/img/logo_blog.png" class="card-img-top card-img" alt="...">
                            @endif   
                            <div class="card-body card-magazine">
                                <h5 class="card-title card-title">{{$magazine->title}}</h5>
                                <p class="card-text">{{$magazine->description}}</p>
                                <a href="{{route('magazine.show', compact("magazine"))}}" class="btn btn-primary button-nav">Dettaglio</a>
                                @if ($magazine->user->id == Auth::id())

                                <a href="{{route('magazine.edit', compact("magazine"))}}" class="btn btn-primary button-nav">Modifica</a>
                                @endif
                            </div>
                    </div>
                @endforeach
            </div>
        </div>
    
</x-layout>