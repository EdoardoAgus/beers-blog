<x-layout>
    <h1 class="title">Condividi le tue idee</h1>
    <div class="container">
      <!-- /resources/views/post/create.blade.php -->


          
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-12 col-md-8">
                <form method="POST" action="{{route('magazine.update', compact('magazine'))}}" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="mb-3">
                      <label  class="form-label">Titolo</label>
                      <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$magazine->title}}">
                      
                    </div>
                    
                    <div class="mb-3">
                      
                      <img src="{{Storage::url($magazine->img)}}" class="img-fluid"alt="">
                      <label for="image" class="form-label"></label>
                      <input type="file" name="img" class="form-control" id="image">
                    </div>
                    <div class="mb-3">
                      <textarea name="description" id="" cols="30" rows="10" value="{{$magazine->description}}"></textarea>
                    </div>
                    <div class="mb-3">
                      <label  class="form-label">Articoli</label>
                      <select name="article">
                        <option value="">Nessun articolo</option>
                        @foreach ($articles as $article)
                        <option value="{{$article->id}}">{{$articles->title}}</option>
                        @endforeach
                        
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary button-filler mb-5">Condividi</button>
                  </form>
            </x-layout>
            </div>
        </div>
    </div>