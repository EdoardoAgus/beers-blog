<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="title">BEARBeer</h1>
            </div>
        </div>
        
    </div>
    <div class="container-flow">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="detail-card">
                 
                        @if ($magazine->img) 
                            <img src="{{Storage::url($magazine->img)}}" class="card-img-top" alt="...">

                        
                        @else 
                    
                        <img src="/img/logo_blog.png" class="card-img-top" alt="...">
                        @endif   
                        <div class="card-body">
                        <h5 class="card-title title">{{$magazine->title}}</h5>
                        <div>
                            <p class="card-text">{{$magazine->description}}</p>
                        </div>
                        <div>
                        <a href="{{route('magazine.auth', ['auth'->magazine->user->id])}}"> <p class="card-text">{{$magazine->user->name}}</p></a>
                        <p>Articoli nella nostra rivista</p>
                        @if (count($magazine->articles) > 0)
                            <ul>
                                @foreach ($magazine->articles as $article)
                                    <li>{{$article->title}}</li>
                                @endforeach
                            </ul>
                        @endif
                        </div>
                        <a href="{{route('homepage')}}" class="btn btn-primary button-filler p-0">Home</a>
                        @if ($magazine->user->id == Auth::id())
                       
                        <form method="POST" action="{{route('magazine.destroy', compact('magazine'))}}">
                            @csrf
                            @method('delete')
                            <button type="submit" class="button-filler"> Cancella</button>
                        </form>
                        @endif
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</x-layout>