<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="title">BEARBeer</h1>
            </div>
        </div>
        
    </div>
    <div class="container-flow">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="detail-card">
                 
                        @if ($article->img) 
                            <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="...">

                        
                        @else 
                    
                        <img src="/img/logo_blog.png" class="card-img-top" alt="...">
                        @endif   
                        <div class="card-body">
                        <h5 class="card-title title">{{$article->title}}</h5>
                        <div>
                            <p class="card-text">{{$article->description}}</p>
                        </div>
                        <div>
                            <p class="card-text mb-3">Di {{$article->user->name}}</p>
                        </div>
                        <div class="mb-3 mt-3">
                            @if (count($article->magazines)>0)
                            <ul>
                                @foreach ($article->magazines as $magazine)
                                    <li>{{$magazine->title}}</li>
                                @endforeach
                            </ul> 
                            @endif
                            
                        </div>
                        <a href="{{route('homepage')}}" class="btn btn-primary button-filler p-0">Home</a>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</x-layout>